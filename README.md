# Microbench

A set of classic benchmarks that I modified to run on modern linux boxes.

## Benchmarks

* Dhrystone, V2.1, C language
* Whetstone
* Linpack
* STREAM

## Branches

* main
  * This branch should work well for most modernish systems
* dhry-intonly
  * Dhrystone is modified to run on integer-only environments.
  * Modifications to work with BTRON environment is included.

## See also

* [Benchmark Heaven II](https://www.okqubit.net/bench/)
  * Results of these benchmarks is published here.

## License

Please follow the original author's license terms, as included in the source file.
